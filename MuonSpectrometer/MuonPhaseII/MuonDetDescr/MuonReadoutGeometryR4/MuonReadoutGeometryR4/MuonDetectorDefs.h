/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONGEOMODELR4_MUONDETECTORDEFS_H
#define MUONGEOMODELR4_MUONDETECTORDEFS_H

#include <GeoPrimitives/GeoPrimitivesHelpers.h>
#include <GeoPrimitives/GeoPrimitivesToStringConverter.h>
///
#include <ActsGeometryInterfaces/ActsGeometryContext.h>

#include <CxxUtils/ArrayHelper.h>
#include <CxxUtils/StringUtils.h>
#include <CxxUtils/bitscan.h>


#include <Identifier/Identifier.h>
#include <Identifier/IdentifierHash.h>

#include <functional>

//// This header contains common helper utilities and definitions
namespace MuonGMR4 {   
 
}  // namespace MuonGMR4

#endif