/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../TgcDigitizationTool.h"

DECLARE_COMPONENT(TgcDigitizationTool)