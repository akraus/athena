/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONTESTER_ARRAYBRANCH_IXX
#define MUONTESTER_ARRAYBRANCH_IXX

#include <MuonTesterTree/MuonTesterTree.h>
#include <MuonTesterTree/throwExcept.h>
#include <format>
namespace MuonVal {
template <class T>
ArrayBranch<T>::ArrayBranch(TTree* t, const std::string& b_name, size_t size) :
    MuonTesterBranch{t,b_name},
    m_size{size} {
    m_data.resize(size);
    m_updated.resize(size);
    reset();
}
template <class T>
ArrayBranch<T>::ArrayBranch(MuonTesterTree& t, const std::string& b_name, size_t size) : 
    MuonTesterBranch{t, b_name},
    m_size{size} {
    m_data.resize(size);
    m_updated.resize(size);
    reset();
}

template <class T>
ArrayBranch<T>::ArrayBranch(TTree* tree, const std::string& name, size_t size, const T& def_val) : 
    ArrayBranch(tree, name, size) {
    setDefault(def_val);
}
template <class T>
ArrayBranch<T>::ArrayBranch(MuonTesterTree& tree, const std::string& name, size_t size, const T& def_val) : 
    ArrayBranch(tree, name, size) {
    setDefault(def_val);
}
template <class T> void ArrayBranch<T>::reset() {
    for (size_t i = 0; i < size(); ++i) m_updated[i] = false;
}
template <class T> const T& ArrayBranch<T>::operator[](size_t s) const { return get(s); }
template <class T> void ArrayBranch<T>::set(size_t s, const T& val) {
    if (s >= size()) { 
        THROW_EXCEPTION("Index "<<s<<" is out of range for "<<name());
    }
    m_updated[s] = true;
    m_data[s] = val;
}
template <class T> const T& ArrayBranch<T>::get(size_t s) const {
    if (s >= size()) { 
        THROW_EXCEPTION("Index "<<s<<" is out of range for "<<name());
    }
    return m_data[s];
}
template <class T> T& ArrayBranch<T>::operator[](size_t s) {
    if (s >= size()) { 
        THROW_EXCEPTION("Index "<<s<<" is out of range for "<<name());
    }
    m_updated[s] = true;
    return m_data[s];
}
template <class T> size_t ArrayBranch<T>::size() const { return m_size; }
template <class T> bool ArrayBranch<T>::init() {
    if (initialized()) {
        ATH_MSG_WARNING("init() -- The branch " << name() << " is already initialized. ");
        return true;
    }
    const std::string br_name{std::format("{:}[{:d}]{:}", name(), size(), tree_data_type())};
    if (name().empty() || !tree()) {
        ATH_MSG_ERROR("init() -- Empty names are forbidden. ");
        return false;
    } else if (tree()->FindBranch(name().c_str())) {
        ATH_MSG_ERROR("init() -- The branch " << name() << " already exists in TTree  " << tree()->GetName() << ".");
        return false;
    } else if (!tree()->Branch(br_name.c_str(), m_data.data())) {
        ATH_MSG_ERROR("init() -- Could not create branch " << name() << " in TTree " << tree()->GetName());
        return false;
    }
    m_init = true;
    return true;
}
template <class T> bool ArrayBranch<T>::initialized() const { return m_init; }
template <class T> bool ArrayBranch<T>::fill(const EventContext&) {
    if (!initialized()) {
        ATH_MSG_ERROR("init()  -- The branch " << name() << " is not initialized yet.");
        return false;
    }
    for (size_t i = 0; i < size(); ++i) {
        if (!m_updated[i]) {
            if (m_failIfNotUpdated) {
                ATH_MSG_ERROR("init()  -- The " << i << "-th value is has not been updated. ");
                return false;
            } else {
                m_data[i] = m_default;
            }
        }
    }
    reset();
    return true;
}
template <class T> const T& ArrayBranch<T>::getDefault() const { return m_default; }
template <class T> void ArrayBranch<T>::setDefault(const T& val) {
    m_default = val;
    m_failIfNotUpdated = false;
}
}
#endif
