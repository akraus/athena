/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/**

@page egammaTools_page egammaTools Package
This package contains electron and photon reconstruction and identification tools.
These tools are called by main algorithms located in the egammaAlgs package.

*/
