# A note on the format of this file
# Lines beginning with a '#' character are comments and will be ignored, as will empty lines
# Triggers are broken up into sections to make it easier to read, and when adding new triggers
# please try and keep them in alphabetical order

#####################
# Muon triggers     #
#####################

############################
# Electron triggers        #
# Legacy EM                #
# Used in 1st part of 2023 #
############################

HLT_2e17_lhvloose_L12EM15VHI
HLT_2e24_lhvloose_L12EM20VH
HLT_e24_lhvloose_2e12_lhvloose_L1EM20VH_3EM10VH
HLT_e26_lhtight_e14_etcut_probe_50invmAB130_L1EM22VHI
HLT_e26_lhtight_e14_idperf_tight_nogsf_probe_50invmAB130_L1EM22VHI
HLT_e26_lhtight_e14_idperf_tight_probe_50invmAB130_L1EM22VHI
HLT_e26_lhtight_ivarloose_e14_idperf_tight_probe_L1EM22VHI
HLT_e26_lhtight_ivarloose_e30_lhloose_nopix_lrtmedium_probe_L1EM22VHI
HLT_e26_lhtight_ivarloose_e30_lhloose_nopix_probe_L1EM22VHI
HLT_e26_lhtight_ivarloose_e4_etcut_probe_L1EM22VHI
HLT_e26_lhtight_ivarloose_e5_idperf_loose_lrtloose_probe_L1EM22VHI
HLT_e26_lhtight_ivarloose_e5_lhtight_probe_L1EM22VHI
HLT_e26_lhtight_ivarloose_e7_lhmedium_probe_L1EM22VHI
HLT_2e17_lhvloose_g20_tight_probe_L12EM15VHI
HLT_2e17_lhvloose_g25_medium_probe_L12EM15VHI
HLT_2e17_lhvloose_g50_loose_probe_L12EM15VHI
HLT_2e24_lhvloose_g20_tight_probe_L12EM20VH
HLT_2e24_lhvloose_g25_medium_probe_L12EM20VH
HLT_2e24_lhvloose_g50_loose_probe_L12EM20VH
HLT_2e12_lhloose_mu10_L12EM8VH_MU8F
HLT_e140_lhloose_L1EM22VHI
HLT_e140_lhloose_noringer_L1EM22VHI
HLT_e26_lhtight_ivarloose_L1EM22VHI
HLT_e30_lhloose_nopix_lrtmedium_L1EM22VHI
HLT_e300_etcut_L1EM22VHI
HLT_e60_lhmedium_L1EM22VHI
HLT_e24_lhmedium_g12_loose_g12_loose_02dRAB_02dRAC_02dRBC_L1EM20VH_3EM10VH
HLT_e24_lhmedium_g25_medium_02dRAB_L12EM20VH
HLT_e25_mergedtight_g35_medium_90invmAB_02dRAB_L12EM20VH
HLT_e26_lhtight_ivarloose_2j20_0eta290_020jvt_boffperf_pf_ftf_L1EM22VHI
HLT_e26_lhtight_ivarloose_j20_pf_ftf_L1EM22VHI
HLT_e12_lhloose_2mu10_L12MU8F
HLT_e7_lhmedium_mu24_L1MU14FCH
HLT_e24_lhmedium_ivarloose_tau20_mediumRNN_tracktwoMVA_03dRAB_L1EM22VHI
HLT_e26_lhtight_ivarloose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI
HLT_e70_lhloose_xe70_cell_L1EM22VHI
HLT_2g22_tight_L12EM15VHI
HLT_2g22_tight_L1EM7_EMPTY
HLT_2g22_tight_L1EM7_UNPAIRED_ISO
HLT_2g25_loose_g15_loose_L12EM20VH
HLT_2g50_loose_L12EM20VH
HLT_g35_medium_g25_medium_L12EM20VH
HLT_g35_medium_g25_medium_L1EM7_EMPTY
HLT_g35_medium_g25_medium_L1EM7_UNPAIRED_ISO
HLT_g140_loose_L1EM22VHI
HLT_g300_etcut_L1EM22VHI
HLT_g50_loose_xe40_cell_xe70_pfopufit_80mTAC_L1EM22VHI
HLT_g50_loose_xe40_cell_xe70_pfopufit_L1EM22VHI
HLT_g50_tight_xe40_cell_xe50_pfopufit_80mTAC_L1EM22VHI
HLT_g90_loose_xe90_cell_L1EM22VHI
HLT_g25_medium_4j35a_j0_DJMASS1000j35_L1EM22VHI
HLT_g35_tight_3j25_pf_ftf_PhysicsTLA_L1EM22VHI
HLT_g35_tight_noiso_3j25_pf_ftf_PhysicsTLA_L1EM22VHI
HLT_g45_tight_2j50_0eta200_emergingPTF0p1dR0p4_pf_ftf_L1EM22VHI
HLT_g85_tight_3j50_L1EM22VHI
HLT_g85_tight_3j50_pf_ftf_L1EM22VHI
HLT_g35_loose_mu15_mu2noL1_L1EM24VHI
HLT_g35_loose_mu18_L1EM24VHI
HLT_g35_tight_mu18noL1_L1EM24VHI
HLT_g140_loose_tau20_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI
HLT_g25_medium_tau25_dikaonmass_tracktwoMVA_50invmAB_L1EM22VHI
HLT_g25_medium_tau25_dipion1_tracktwoMVA_50invmAB_L1EM22VHI
HLT_g25_medium_tau25_kaonpi1_tracktwoMVA_50invmAB_L1EM22VHI
HLT_g25_medium_tau25_singlepion_tracktwoMVA_50invmAB_L1EM22VHI
HLT_g35_medium_tau25_dipion3_tracktwoMVA_L1TAU12_60invmAB_L1EM22VHI

#####################
# Photon triggers   #
#####################

#########################################################################
# Tau triggers                                                          #
# Lowest unprescaled di-tau items in physics_Main are listed explicitly #
# as the algorithmic determined items only feed the delayed stream.     #
# Also include legacy as for egamma, above. Also include tag and probe  #
# triggers used for tau trigger scale factor measurement                #
#########################################################################

HLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_03dRAB30_L1cTAU30M_2cTAU20M_DR-eTAU30eTAU20-jJ55
HLT_tau35_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_03dRAB30_L1DR-TAU20ITAU12I-J25
HLT_tau160_mediumRNN_tracktwoMVA_L1eTAU140
HLT_tau80_mediumRNN_tracktwoMVA_tau60_mediumRNN_tracktwoMVA_03dRAB_L1eTAU80_2eTAU60
HLT_tau80_mediumRNN_tracktwoMVA_tau60_mediumRNN_tracktwoMVA_03dRAB_L1TAU60_2TAU40

# The 2024 primary b+tau chain needs listing explicitly as three-signature (multij, bj, tau) chains are not currently supported in the TriggerAPI

HLT_tau25_mediumRNN_tracktwoMVA_probe_L1eTAU12_j65c_020jvt_j40c_020jvt_j25c_020jvt_j20c_020jvt_SHARED_j20c_020jvt_bgn285_pf_ftf_presel3c20XX1c20bgtwo85_L1jJ85p0ETA21_3jJ40p0ETA25

# Tag and probe triggers
# mu+tau 2022+2023
HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH
HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH
HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH
HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH
HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH
HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1MU14FCH
# mu +tau 2024
HLT_mu24_ivarmedium_tau25_mediumRNN_tracktwoMVA_probe_L1cTAU20M_03dRAB_L1MU14FCH
HLT_mu26_ivarmedium_tau30_mediumRNN_tracktwoMVA_probe_L1cTAU20M_03dRAB_L1MU14FCH
HLT_mu24_ivarmedium_tau35_mediumRNN_tracktwoMVA_probe_L1cTAU30M_03dRAB_L1MU14FCH
HLT_mu24_ivarmedium_tau40_mediumRNN_tracktwoMVA_probe_L1cTAU35M_03dRAB_L1MU14FCH
HLT_mu24_ivarmedium_tau60_mediumRNN_tracktwoMVA_probe_L1eTAU60_03dRAB_L1MU14FCH
HLT_mu24_ivarmedium_tau80_mediumRNN_tracktwoMVA_probe_L1eTAU80_03dRAB_L1MU14FCH
HLT_mu24_ivarmedium_tau160_mediumRNN_tracktwoMVA_probe_L1eTAU140_03dRAB_L1MU14FCH
# elec + tau 2022
HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI
HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI
HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI
HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI
HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI
HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_03dRAB_L1EM22VHI
# elec + tau 2023
HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_L1TAU12IM_03dRAB_L1eEM26M
HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_L1TAU20IM_03dRAB_L1eEM26M
HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_L1TAU25IM_03dRAB_L1eEM26M
HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_L1TAU40_03dRAB_L1eEM26M
HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_L1TAU60_03dRAB_L1eEM26M
HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_L1TAU100_03dRAB_L1eEM26M
# elec + tau 2024
HLT_e26_lhtight_ivarloose_tau25_mediumRNN_tracktwoMVA_probe_L1cTAU20M_03dRAB_L1eEM26M
HLT_e26_lhtight_ivarloose_tau30_mediumRNN_tracktwoMVA_probe_L1cTAU20M_03dRAB_L1eEM26M
HLT_e26_lhtight_ivarloose_tau35_mediumRNN_tracktwoMVA_probe_L1cTAU30M_03dRAB_L1eEM26M
HLT_e26_lhtight_ivarloose_tau40_mediumRNN_tracktwoMVA_probe_L1cTAU35M_03dRAB_L1eEM26M
HLT_e26_lhtight_ivarloose_tau60_mediumRNN_tracktwoMVA_probe_L1eTAU60_03dRAB_L1eEM26M
HLT_e26_lhtight_ivarloose_tau80_mediumRNN_tracktwoMVA_probe_L1eTAU80_03dRAB_L1eEM26M
HLT_e26_lhtight_ivarloose_tau160_mediumRNN_tracktwoMVA_probe_L1eTAU140_03dRAB_L1eEM26M

#####################
# Jet triggers      #
#####################

#####################
# BJet triggers     #
#####################
